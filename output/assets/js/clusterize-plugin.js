function clusterizeTable(options) {

    var clusterizeTableParentDivId = options.parentDivId;
    var clusterizeTableClassName = options.classSelector;
  
    if(document.getElementById(clusterizeTableParentDivId) == null) {
      return;
    }
      
  
    var parentDiv = document.getElementById(clusterizeTableParentDivId);
    var selectedTables = parentDiv.getElementsByClassName(clusterizeTableClassName);
  
      Array.from(selectedTables).forEach((contentTable, index) => {
          var uniqueIdSuffix = clusterizeTableClassName + '_' + index;
          var tableData = contentTable.getElementsByTagName('tbody')[0].innerHTML;
          var tableRowData = tableData.split('</tr>');
  
          for (var i = 0; i < tableRowData.length; i++) {
              if (tableRowData[i].slice(-1) != '</tr>') {
                  tableRowData[i] = tableRowData[i] + '</tr>';
              }
          }
  
          var clusterizePanel = document.createElement('div'); //'<div class="panel th">'
          clusterizePanel.className = 'panel th';
  
          var clusterizeDivEl = '<div class="clusterize">' +
              '<input type="text" placeholder="Type here for search" class="form-control search-table" id="search_' + uniqueIdSuffix + '">' +
              '<div class="clusterize-headers">' +
              '<table class="fixed-header" id="headersArea_' + uniqueIdSuffix + '"></table>' +
              '</div>' +
              '<div id="scrollArea_' + uniqueIdSuffix + '" class="clusterize-scroll">' +
              '<table class="' + clusterizeTableClassName + ' clusterize-table" id="contentTable_' + uniqueIdSuffix + '">' +
              '<thead id="hiddenHeader_' + uniqueIdSuffix + '">' + tableRowData[0] + '</thead>' +
              '<tbody id="domContentArea_' + uniqueIdSuffix + '" style="width: 100%;"></tbody></table>' +
              '</div>' +
              '</div>';
  
          clusterizePanel.innerHTML = clusterizeDivEl
  
  
          //var tableHeaderData = tableRowData[0]
          tableRowData = tableRowData.splice(1, tableRowData.length);
          //contentTable.innerHTML = "<tbody id='domContentArea'></tbody>";
          //contentTable.style.display = "";
          contentTable?.replaceWith?.(clusterizePanel);
  
          document.getElementById("contentTable_" + uniqueIdSuffix).style.display = "table";
  
          // Below Codes are for setting header in sync with table body
  
          var $scroll = $("#scrollArea_" + uniqueIdSuffix);
          var $content = $('#domContentArea_' + uniqueIdSuffix);
          var $hiddenHeader = $("#hiddenHeader_" + uniqueIdSuffix);
          var $headers = $("#headersArea_" + uniqueIdSuffix);
  
          //console.log($headers);
          //console.log(tableHeaderData);
  
          //$headers.html('<thead style="min-width:100%">'+ $hiddenHeader.html() + '</thead>');
  
          /**
           * Makes header columns equal width to content columns
           */
          var fitHeaderColumns = (function() {
              var prevWidth = [];
              return function() {
                  var $firstRow = $hiddenHeader.find('tr'); //$content.find('tr:not(.clusterize-extra-row):first');
                  var columnsWidth = [];
                  $firstRow.children().each(function() {
                      columnsWidth.push($(this).width());
                  });
                  //if (columnsWidth.toString() == prevWidth.toString()) return;
                  $headers.find('tr').children().each(function(i) {
                      $(this).width(columnsWidth[i]);
                  });
                  //prevWidth = columnsWidth;
              }
          })();
  
          /**
           * Keep header equal width to tbody
           */
          var setHeaderWidth = function() {
              $headers.width($content.width());
          }
  
          /**
           * Set left offset to header to keep equal horizontal scroll position
           */
          var setHeaderLeftMargin = function(scrollLeft) {
              $headers.css('margin-left', -scrollLeft);
          }
  
          var updateHeader = function() {
              $("#headersArea_" + uniqueIdSuffix).html('');
              $("#headersArea_" + uniqueIdSuffix).html('<thead>' + $("#hiddenHeader_" + uniqueIdSuffix).html() + '</thead>');
              $("#hiddenHeader_" + uniqueIdSuffix).css('visibility', 'hidden');
              $('#contentTable_' + uniqueIdSuffix).css('margin-top', -($("#hiddenHeader_" + uniqueIdSuffix).outerHeight()))
              fitHeaderColumns();
              setHeaderWidth();
          }
  
          // header sync code ends
  
          var clusterize = new Clusterize({
              rows: tableRowData,
              scrollId: 'scrollArea_' + uniqueIdSuffix,
              contentId: 'domContentArea_' + uniqueIdSuffix,
              rows_in_block: 15,
              blocks_in_cluster: 4,
              show_no_data_row:true,
              no_data_text: 'Loading Data...',
              callbacks: {
                  clusterChanged: function() {
                      setTimeout(updateHeader(), 1000);
                  }
              }
          });
  
          //clusterize.clear();
  
          //clusterize.update(tableRowData);
          //console.log(tableRowData);
  
          var search = document.getElementById('search_' + uniqueIdSuffix);
  
          //var searchData = Object.assign([], tableRowData);
  
          var parseChildNodesForStringMatch = function(elem, query) {
              if (elem.getElementsByTagName('td').length > 0) {
                  var tdCollection = elem.getElementsByTagName('td');
                  for (var i = 0; i < tdCollection.length; i++) {
                      let child = tdCollection[i];
                      const keywordRegex = RegExp(query, 'gi');
                      if (keywordRegex.test(child.textContent)) {
                          return true
                      }
                  }
              }
  
              return false;
          };
  
          var htmlStringMatchesQuery = (str, query) => {
  
              let tempTable = document.createElement('table');
              tempTable.innerHTML = str;
  
              return parseChildNodesForStringMatch(tempTable, query);
          };
  
          let timer;              // Timer identifier
        //   let waitTime = 16;   // Wait time in milliseconds 
  
          var onSearch = function () {
              waitTime = tableRowData.length/16;
              clusterize.clear();
              clusterize.update([]);
              clearTimeout(timer);
              timer = setTimeout(() => {
                  var searchData = [];
  
                  if (search.value.trim() == "") {
                      //clusterize.destroy(true);
                      clusterize.clear();
                      searchData = Object.assign([], tableRowData);
                      clusterize.update(tableRowData);
  
                  } else {
  
                      for (var j = 0, jj = tableRowData.length; j < jj; j++) {
                          //if (tableRowData[j].toString().toLowerCase().indexOf(search.value.toLowerCase().trim()) + 1) {
                          if (htmlStringMatchesQuery(tableRowData[j], search.value.trim())) {
                              var fileredRow = tableRowData[j];
  
                              searchData.push(fileredRow);
                          }
                      }
                      //if(searchData.length == tableRowData.length + 1)
                      //  searchData = searchData.splice(1, searchData.length);
                      //console.log(searchData);
                      //clusterize.destroy(true);
                      clusterize.clear();
                      clusterize.update(searchData);
                      highlight(document.getElementById('domContentArea_' + uniqueIdSuffix), [search.value]);
  
                  }
                  if(searchData.length == 0) {
                      document.getElementById('domContentArea_' + uniqueIdSuffix)
                      .getElementsByClassName('clusterize-no-data')[0]
                      .getElementsByTagName('td')[0].innerHTML = 'No Data Found';
                  }
              }, waitTime);
  
          }
  
          search.oninput = onSearch;
          
  
  
          /**
           * Update header columns width on window resize
           */
  
          $(window).resize(_.debounce(fitHeaderColumns, 150));
  
          /**
           * Update header left offset on scroll
           */
          $scroll.on('scroll', (function() {
              var prevScrollLeft = 0;
              return function() {
                  var scrollLeft = $(this).scrollLeft();
                  if (scrollLeft == prevScrollLeft) return;
                  prevScrollLeft = scrollLeft;
  
                  setHeaderLeftMargin(scrollLeft);
  
              }
          }()));
  
      });
  
  
      /**
       * Highlight keywords inside a DOM element
       * @param {string} elem Element to search for keywords in
       * @param {string[]} keywords Keywords to highlight
       * @param {boolean} caseSensitive Differenciate between capital and lowercase letters
       * @param {string} cls Class to apply to the highlighted keyword
       */
      function highlight(elem, keywords, caseSensitive = true, cls = 'highlight') {
          const flags = caseSensitive ? 'gi' : 'g';
          // Sort longer matches first to avoid
          // highlighting keywords within keywords.
          keywords.sort((a, b) => b.length - a.length);
          Array.from(elem.childNodes).forEach(child => {
              const keywordRegex = RegExp(keywords.join('|'), flags);
              if (child.nodeType !== 3) { // not a text node
                  highlight(child, keywords, caseSensitive, cls);
              } else if (keywordRegex.test(child.textContent)) {
                  const frag = document.createDocumentFragment();
                  let lastIdx = 0;
                  child.textContent.replace(keywordRegex, (match, idx) => {
                      const part = document.createTextNode(child.textContent.slice(lastIdx, idx));
                      const highlighted = document.createElement('span');
                      highlighted.textContent = match;
                      highlighted.classList.add(cls);
                      frag.appendChild(part);
                      frag.appendChild(highlighted);
                      lastIdx = idx + match.length;
                  });
                  const end = document.createTextNode(child.textContent.slice(lastIdx));
                  frag.appendChild(end);
                  child.parentNode.replaceChild(frag, child);
              }
          });
      }
  
  }